import java.util.LinkedList;

public class HashTable {
    // 해쉬 테이블 만든 이유 - 하나로 묶어 줄려고
    private LinkedList<Node>[] list;

    public HashTable(int size) {
        this.list = new LinkedList[size];
    }

    public int getHash(String key) { // K,J,M
        int totalHash = 0;
        for (char c : key.toCharArray()) totalHash += c;
        return totalHash;
    }

    public int getRoomNumber(int hash) {
        return hash % list.length;
    }

    // 찾으려면 어디 방에 있는지와 찾을 키가 있어야 함
    public Node searchNode(LinkedList<Node> list, String key) {
        if (list == null) return null;

        for (Node node : list) {
            if (node.getKey().equals(key)) return node;
        }

        // 찾아보고 있을 경우만 줄거기 때문에 null을 리턴한다
        return null;
    }

    // 값 수정이나 등록
    public void setData(String key, String value) {
        int hash = getHash(key);
        int roomIndex = getRoomNumber(hash);

        // 기존 인간지네 가져오는 작업
        LinkedList<Node> nodes = list[roomIndex];

        // 방 번호가 없으면 새로 만든다
        if (nodes == null) {
            nodes = new LinkedList<>();
            list[roomIndex] = nodes;
        }

        Node node = searchNode(nodes, key);
        if (node == null) nodes.add(new Node(key, value));
        else node.setValue(value);
    }

    public String getNodeValue(String key) {
        int hash = getHash(key);
        int roomIndex = getRoomNumber(hash);
        LinkedList<Node> nodes = list[roomIndex];
        Node node = searchNode(nodes, key);

        return node == null ? "No Data" : node.getValue();
    }

    // public static 공개 고정 - 메모리상에 고정(static zone)영역이 따로 있음. 그럼 모두의 것이 댐
    public static class Node {
        private String key;
        private String value;
        public Node(String key, String value) {
            this.key = key;
            this.value = value;
        }
        public String getKey() {
            return this.key;
        }

        public String getValue() {
            return this.value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}