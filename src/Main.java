// Hash Table
//public class Main {
//    public static void main(String[] args) {
//        HashTable line607 = new HashTable(3);
//        line607.setData("MMR", "잠자기");
//        line607.setData("KBJ", "드라이브" );
//
//        System.out.println(line607.getNodeValue("KBJ"));
//    }
//}

// Hash Map
//import java.util.HashMap;
//import java.util.LinkedList;
//
//public class Main {
//    public static void main(String[] args) {
//        HashMap<String, LinkedList<String>> qna607 = new HashMap<>();
//        HashMap<String, LinkedList<String>> qna608 = new HashMap<>();
//
//        // 데이터 넣는 것
////        qna.put("DDONG_O", new LinkedList<>());
//        // 밀어 널을 껀데 있냐고 물어보는 것, 키가 같은게 있으면 안들어감
//        qna607.putIfAbsent("DDONG_O", new LinkedList<>());
//        qna607.putIfAbsent("DDONG_X", new LinkedList<>());
//        qna607.putIfAbsent("SIDE_O", new LinkedList<>());
//        qna607.putIfAbsent("SIDE_X", new LinkedList<>());
//
//        // 위에꺼랑 같은 구조로 608을 한번에 만들고 싶을 때
//        qna608.putAll(qna607);
//
//        // 그냥 리플이스를 쓰면 링크드 리스트 자체가 교체 된다
//        // 따라서 밑에처럼 불러와서 바꿔줘야 한다
//        LinkedList<String> ddongOStudents = qna607.get("DDONG_O");
//        ddongOStudents.add("김기범");
//        ddongOStudents.add("최재오");
//        ddongOStudents.add("최연진");
//
//        System.out.println(qna607.size());
//
//        System.out.println(ddongOStudents.size());
//
//        System.out.println(qna607);
//    }
//}

// 완주 하지 못한 선수 (Hash Map)
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Objects;
//
//public class Main {
//    public static void main(String[] args) {
//        String[] participant = {"mislav", "stanko", "mislav", "ana"};
//        String[] completion = {"stanko", "ana", "mislav"};
//
//        System.out.println(solution(participant, completion));
//    }
//
//    public static String solution(String[] participant, String[] completion) {
//        HashMap<String, Integer> map = new HashMap<>();
//
//        for (String s : participant) {
//            if (map.containsKey(s)) map.put(s, map.get(s) + 1);
//            else map.put(s, 1);
//        }
//
////        for (String s : participant) map.put(s, map.getOrDefault(s, 0) + 1);
//        for (String s : completion) map.put(s, map.get(s) - 1);
//
//        return map.entrySet().stream().filter(entry -> Objects.equals(entry.getValue(), 1)).map(Map.Entry::getKey).findFirst().orElse("");
////        return map.entrySet().stream().filter(entry -> entry.getValue() != 0).map(Map.Entry::getKey).findFirst().orElse("");
//    }
//}

// 폰켓몬 (Hash Map)
//import java.util.HashMap;
//
//public class Main {
//    public static void main(String[] args) {
//        int[] nums = {3,3,3,2,2,2};
//
//        System.out.println(solution(nums));
//    }
//
//    public static int solution(int[] nums) {
//        HashMap<Integer, Integer> map = new HashMap<>();
//
//        for (int p : nums) map.put(p, 1);
//
//        return Math.min(nums.length / 2, map.size());
//    }
//}

// Iterator 값만 있는 것
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//
//public class Main {
//    public static void main(String[] args) {
//        List<String> testList = new LinkedList<>();
//        testList.add("홍길동");
//        testList.add("박길동");
//        testList.add("고길동");
//
//        // 리스트를 순번을 줘서 복제 뜨니까 리스트와 비슷한 애 따라서 <>에 String만 들어감
//        Iterator<String> iterator = testList.iterator();
//        // hasNext, next, remove밖에 못하니까 while문 써야함
//        while (iterator.hasNext()) {
//            String resultValue = iterator.next();
//            System.out.println(resultValue);
//        }
//    }
//}

//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.TreeMap;
//
//// Iterator key:value 형태
////public class Main {
////    public static void main(String[] args) {
////        HashMap<String, String> map = new HashMap<>();
////        map.put("홍길동", "똥을 좋아해");
////        map.put("박길동", "똥 안 좋아해");
////        map.put("오기리", "별로 관심 없어");
////
////        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
////        while (iterator.hasNext()) {
////            Map.Entry<String, String> item = iterator.next();
////            System.out.println(item.getKey() + "은 " + item.getValue());
////        }
////    }
////}
//
//// Tree Map
//public class Main {
//    public static void main(String[] args) {
//        HashMap<String, String> hashMap = new HashMap<>();
//        hashMap.put("홍길동", "똥을 좋아해");
//        hashMap.put("박길동", "똥 안 좋아해");
//        hashMap.put("오기리", "별로 관심 없어");
//
//        TreeMap<Double, String> fingerNailMap = new TreeMap<>();
//        fingerNailMap.put(1.34D, "문미림");
//        fingerNailMap.put(1.6D, "최재오");
//        fingerNailMap.put(2D, "배병수");
//        fingerNailMap.put(1.8548373D, "권봉주");
//
//        fingerNailMap.entrySet().stream().forEach(e -> System.out.println(e.getValue()));
//    }
//}

// 숫자 짝꿍
//import java.util.Collection;
//import java.util.Collections;
//import java.util.TreeMap;
//
//public class Main {
//    public static void main(String[] args) {
//     String x = "12321";
//     String y = "42531";
//     System.out.println(solution(x, y));
//    }
//
//    public static String solution(String x, String y) {
//        // 내림차순 할꺼니까 미리 collections reverseOrder를 한다
//        TreeMap<Character, Integer> mapX = new TreeMap<>();
//        TreeMap<Character, Integer> mapY = new TreeMap<>();
//
//        // 문자를 배열로 만들고 캐릭터로 하나씩 던짐, 키 비교해서 같은게 없으면 0 + 1, 있으면 기존값 + 1
//        for (char c : x.toCharArray()) mapX.put(c, mapX.getOrDefault(c, 0) + 1);
//        for (char c : y.toCharArray()) mapY.put(c, mapY.getOrDefault(c, 0) + 1);
//
//        // 스트링 빌더로 문자를 하나씩 쌓는다
//        StringBuilder sb = new StringBuilder();
//        // 아스키 코드로 어차피 내림차순 할꺼니까 9부터 내림
//        for (char i = '9'; i >= '0'; i--) {
//            // mapX와 mapY에 둘다 i가 있을 때
//            if (mapX.containsKey(i) && mapY.containsKey(i)) {
//                // 두개중에 최소값을 찾음
//                int minCount = Math.min(mapX.get(i), mapY.get(i));
//                // j갯수만큼 i를 sb에 붙인다
//                for (int j = 0; j < minCount; j++) sb.append(i);
//            } ;
//        }
//
//        if (sb.isEmpty()) return "-1";
//        else if (sb.charAt(0) == '0') return "0";
//        else return sb.toString();
//    }
//}

//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.Set;
//
////  HashSet
//public class Main {
//    public static void main(String[] args) {
//        // 해쉬셋이 셋을 구현한거임
//        Set<String> ddongLikes = new HashSet<>();
//        ddongLikes.add("문미림");
//        ddongLikes.add("문미림");
//        ddongLikes.add("최연진");
//        ddongLikes.add("최재오");
//        ddongLikes.add("노지은");
//
//        System.out.println(ddongLikes.contains("문미림"));
//
//        Iterator<String> iterator = ddongLikes.iterator();
//        while (iterator.hasNext()) {
//            String name = iterator.next();
//            System.out.println(name);
//        }
//    }
//}

// Tree Set (up casting, down casting)
//import java.util.Set;
//import java.util.TreeSet;
//
//public class Main {
//    public static void main(String[] args) {
//        // 둘의 차이는 뭘까?
//        // 인터페이스도 결국에 부모다
//        // 셋을 구현한 자식을 부모로 했음
//        TreeSet<String> hobbySet = new TreeSet<>();
//        // 트리셋을 셋으로 바꿧음 자식 + a 에서 a를 버림 -> 업캐스팅
//        // 업캐스팅을 한 후에 나중에 다른 자식으로 캐스팅해서 쓰려고
////        Set<String> treeSet2 = new TreeSet<>();
//        // 다운 캐스팅 - 없는 기능 a를 어케 채울지에 대한 문제가 생김
//        // 써야 하는 경우가 있음 - 패키지 만들 때
//        // 업캐스팅 해서 못쓰는 기능을 다시 사용하고 싶을 때
////        TreeSet<String> treeSet3 = (TreeSet<String>) treeSet2;
//
//        hobbySet.add("문미림");
//        hobbySet.add("김남호");
//        hobbySet.add("최연진");
//        hobbySet.add("최재오");
//        hobbySet.add("강민정");
//        hobbySet.add("함수빈");
//        hobbySet.add("김기범");
//        hobbySet.add("이윤진");
//        hobbySet.add("홍수경");
//        // 트리의 특징 - 정렬
//        // 오름차순으로 정렬 될 것이다
//
//        System.out.println(hobbySet);
//        System.out.println(hobbySet.first());
//        System.out.println(hobbySet.last());
//        System.out.println(hobbySet.lower("문미림"));
//        System.out.println(hobbySet.higher("문미림"));
//        hobbySet.pollFirst();
//        hobbySet.pollLast();
//        System.out.println(hobbySet);
//        // 범위로 가져오기
//        System.out.println(hobbySet.subSet("문미림", "최재오"));
//    }
//}

// Linked Hash Set
//import java.util.LinkedHashSet;
//
//public class Main {
//    public static void main(String[] args) {
//        LinkedHashSet<String> likeGameSet = new LinkedHashSet<>();
//        likeGameSet.add("문미림");
//        likeGameSet.add("강권김");
//        likeGameSet.add("김김노");
//
//        System.out.println(likeGameSet);
//    }
//}


// 문자열 압축
//public class Main {
//    public static void main(String[] args) {
//        System.out.println(solution("aabbaccc"));
//    }
//
//    public static int solution(String s) {
//        // 잘라지지 않으면 기존 값의 렝쓰로 해야되서 먼저 선언해줌
//        int minLength = s.length();
//        // 1글자부터 총길이의 반까지만 자를거니까 시작값 1 마지막값 길이의 반
//        for (int length = 1; length <= minLength / 2; length++) {
//            // 최종적으로 쌓아둔 문자와 초기값을 비교해야 해서 스트링 빌더 사용
//            StringBuilder sb = new StringBuilder();
//            // 중간에 문자를 비교 해야 해서 변수 선언
//            String lastPart = "";
//            // 몇개 있는지 확인 해야 해서 카운트 변수 선언
//            int cnt = 0;
//            // 문자열을 자르는 인덱스를 주기 위해 처음 0으로 끝을 s의 길이로 자를 기준 뒤의 값을 위해 length차이 만큼 건너뜀
//            for (int start = 0; start < s.length(); start+=length) {
//                // 자르기 위해 두번째 인자값을 위한 end 변수 선언
//                int end = Math.min(start + length, s.length());
//                // 문자를 잘라 변수로 선언
//                String part = s.substring(start, end);
//
//                // 자른 변수와 비교 변수가 같다면 카운트 올림
//                if (part.equals(lastPart)) cnt++;
//                // 다르면
//                else {
//                    // 스트링 빌더에 추가하는데 카운트가 1보다 크면 숫자와 비교 문자를 합친걸 쌓음 아니면 그냥 비교 문자를 쌓음
//                    sb.append(cnt > 1 ? cnt + lastPart : lastPart);
//                    // 자른 문자를 비교 문자로 넣음
//                    lastPart = part;
//                    // 그러면서 카운트를 1로 바꿈
//                    cnt = 1;
//                }
//            }
//            // for문에선 자른 변수와 비교 변수를 비교해서 다를 때 build에 쌓는데 마지막번째가 같은걸로 끝났을 수도 있어서 추가를 해줘야 함
//            sb.append(cnt > 1 ? cnt + lastPart : lastPart);
//            // 원래 글자 렝쓰랑 잘라서쌓은 sb의 렝쓰를 비교해서 작은걸 minLength에 넣는다
//            minLength = Math.min(minLength, sb.length());
//        }
//
//        return minLength;
//    }
//}

// Skip List Map
//import java.util.concurrent.ConcurrentSkipListMap;
//
//public class Main {
//    public static void main(String[] args) {
//        ConcurrentSkipListMap<String, Integer> breadLikes = new ConcurrentSkipListMap<>();
//        breadLikes.put("배병수", 8934848);
//        breadLikes.put("노지은", 34289712);
//        breadLikes.put("김기범", 1);
//        breadLikes.put("박진수", 0);
//        breadLikes.put("김남호", 999999999);
//        breadLikes.remove("김기범");
//        breadLikes.remove("박진수");
//
//        System.out.println(breadLikes);
//        System.out.println(breadLikes.get("김남호"));
//    }
//}


// Vector 사칙 연산
//import java.util.Vector;
//
//public class Main {
//    public static void main(String[] args) {
//        // 1 : 스칼라
//        // [2, 34, 54] : 벡터
//        // [[13,4], [32,1]] : 행렬
//        Vector<Integer> a = new Vector<>(3); // 배열의 크기 지정
//        a.add(5);
//        a.add(6);
//        a.add(15);  // [5, 6, 15]
//        Vector<Integer> b = new Vector<>(3);
//        b.add(7);
//        b.add(9);
//        b.add(4);   // [7, 9, 4]
//
//        System.out.println(getSum(a, b));
//        System.out.println(getMinus(a, b));
//        System.out.println(getMultiplication(a, b));
//        System.out.println(getDivision(a, b));
//    }
//
//    private static Vector<Integer> getSum(Vector<Integer> vectorA, Vector<Integer> vectorB) {
//        Vector<Integer> result = new Vector<>(vectorA.size());
//        for (int i = 0; i < vectorA.size(); i++) result.add(vectorA.get(i) + vectorB.get(i));
//        return result;
//    }
//
//    private static Vector<Integer> getMinus(Vector<Integer> vectorA, Vector<Integer> vectorB) {
//        Vector<Integer> result = new Vector<>(vectorA.size());
//        for (int i = 0; i < vectorA.size(); i++) result.add(vectorA.get(i) - vectorB.get(i));
//        return result;
//    }
//
//    private static Vector<Integer> getMultiplication(Vector<Integer> vectorA, Vector<Integer> vectorB) {
//        Vector<Integer> result = new Vector<>(vectorA.size());
//        for (int i = 0; i < vectorA.size(); i++) result.add(vectorA.get(i) * vectorB.get(i));
//        return result;
//    }
//
//    private static Vector<Double> getDivision(Vector<Integer> vectorA, Vector<Integer> vectorB) {
//        Vector<Double> result = new Vector<>(vectorA.size());
//        for (int i = 0; i < vectorA.size(); i++) {
//            if (vectorA.get(i) != 0 && vectorB.get(i) != 0) {
//                result.add((double) vectorA.get(i) / vectorB.get(i));
//            } else result.add(Double.NaN);
//        }
//        return result;
//    }
//}

//import java.util.HashMap;
//import java.util.LinkedList;

// 인접 리스트
//public class Main {
//    public static void main(String[] args) {
//        HashMap<String, LinkedList<String>> graph = new HashMap<>();
//        graph.put("A", new LinkedList<>());
//        graph.put("B", new LinkedList<>());
//        graph.put("C", new LinkedList<>());
//        graph.put("D", new LinkedList<>());
//        graph.put("E", new LinkedList<>());
//        graph.put("F", new LinkedList<>());
//        graph.put("G", new LinkedList<>());
//
//        LinkedList<String> valueA = graph.get("A");
//        valueA.add("B");
//        valueA.add("D");
//
//        LinkedList<String> valueB = graph.get("B");
//        valueB.add("A");
//        valueB.add("F");
//        valueB.add("G");
//
//        LinkedList<String> valueC = graph.get("C");
//        valueC.add("D");
//
//        LinkedList<String> valueD = graph.get("D");
//        valueD.add("A");
//        valueD.add("C");
//
//        LinkedList<String> valueE = graph.get("E");
//        valueE.add("F");
//
//        LinkedList<String> valueF = graph.get("F");
//        valueF.add("B");
//        valueF.add("E");
//
//        LinkedList<String> valueG = graph.get("G");
//        valueG.add("B");
//
//        System.out.println(graph);
//    }
//}


//import java.util.*;
//
//// BFS HashMap 인접 리스트
//public class Main {
//    public static void main(String[] args) {
//        // 키로 위치 밸류로 갈수 있는 데를 쓰려고 해쉬맵
//        HashMap<String, LinkedList<String>> map = new HashMap<>();
//        map.put("A", new LinkedList<>());
//        map.put("B", new LinkedList<>());
//        map.put("C", new LinkedList<>());
//        map.put("D", new LinkedList<>());
//        map.put("E", new LinkedList<>());
//        map.put("F", new LinkedList<>());
//
//        LinkedList<String> valueA = map.get("A");
//        valueA.add("B");
//        valueA.add("C");
//
//        LinkedList<String> valueB = map.get("B");
//        valueB.add("A");
//        valueB.add("D");
//        valueB.add("E");
//
//        LinkedList<String> valueC = map.get("C");
//        valueC.add("A");
//        valueC.add("F");
//
//        LinkedList<String> valueD = map.get("D");
//        valueD.add("B");
//
//        LinkedList<String> valueE = map.get("E");
//        valueE.add("B");
//
//        LinkedList<String> valueF = map.get("F");
//        valueF.add("C");
//
//        bfs(map, "A");
//    }
//
//    private static void bfs(HashMap<String, LinkedList<String>> map, String startKey) {
//        // 값을 뺄 때 bfs 순서대로 뽑아야 해서 큐로 함
//        Queue<String> queue = new LinkedList<>();
//        // 방문했는지 안했는지 계속 확인해야 해서 어레이 리스트
//        List<String> visited = new ArrayList<>();
//
//        // 큐로 넣을 때랑 리스트로 넣을 때 구분 하려고 쌤은 큐에 오퍼로 함
//        queue.offer(startKey);
//        visited.add(startKey);
//
//        // 큐에 값이 있을 동안만 돌아야 해서 비어 있니의 반대
//        while (!queue.isEmpty()) {
//            // 큐에서 하나를 뺌
//            String node = queue.poll();
//
//            // (방)링크드 리스트를 불러옴
//            LinkedList<String> neighbors = map.get(node);
//            // 하나씩 던짐
//            for (String neighbor : neighbors) {
//                // 방문 하지 않았으면
//                if (!visited.contains(neighbor)) {
//                    // 큐에 넣고
//                    queue.offer(neighbor);
//                    // 방문 했다고 추가함
//                    visited.add(neighbor);
//                }
//            }
//        }
//
//        System.out.println(visited);
//    }
//}

// BFS 인접 행렬
//public class Main {
//    public static void main(String[] args) {
//        String[] colName = {"A", "B", "C", "D", "E", "F"};
//        int[][] matrix = {
//                {0, 1, 1, 0, 0, 0},
//                {1, 0, 0, 1, 1, 0},
//                {1, 0, 0, 0, 0, 1},
//                {0, 1, 0, 0, 0, 0},
//                {0, 1, 0, 0, 0, 0},
//                {0, 0, 1, 0, 0, 0}
//        };
//
//        bfs(colName, matrix, "A");
//    }
//
//    private static void bfs(String[] colName, int[][] matrix, String start) {
//        // 큐를 준비함
//        Queue<String> queue = new LinkedList<>();
//        // 방문 확인을 위해 리스트 준비함
//        List<String> visited = new ArrayList<>();
//        // 문자로 된 colName을 숫자로 바꿈
//        int startIndex = Arrays.asList(colName).indexOf(start);
//
//        // 큐에 넣는데 A B C로 넣으려고 콜네임의 스타트 인덱스 번호를 넣음
//        queue.offer(colName[startIndex]);
//        visited.add(colName[startIndex]);
//
//        while (!queue.isEmpty()) {
//            String node = queue.poll();
//
//            // 노드의 인덱스를 찾으려고 순서에 맞는 인트로 바꿈
//            int nodeIndex = Arrays.asList(colName).indexOf(node);
//            // 방의 길이 만큼 돎
//            for (int i = 0; i < matrix[nodeIndex].length; i++) {
//                // 방의 노드가 1이면서 방문하지 않았으면 실행
//                if (matrix[nodeIndex][i] == 1 && !visited.contains(colName[i])) {
//                    // 큐에 값 추가
//                    queue.offer(colName[i]);
//                    // 방문 했는지에 값 추가
//                    visited.add(colName[i]);
//                }
//            }
//        }
//        System.out.println(visited);
//    }
//}

import java.util.*;

// DFS 인접 리스트
//public class Main {
//    public static void main(String[] args) {
//        HashMap<String, LinkedList<String>> map = new HashMap<>();
//        map.put("A", new LinkedList<>());
//        map.put("B", new LinkedList<>());
//        map.put("C", new LinkedList<>());
//        map.put("D", new LinkedList<>());
//        map.put("E", new LinkedList<>());
//        map.put("F", new LinkedList<>());
//
//        // 데이터를 반대로 넣거나
//        LinkedList<String> valueA = map.get("A");
//        valueA.add("C");
//        valueA.add("B");
//
//        // addFirst 메서드 쓰거나
//        LinkedList<String> valueB = map.get("B");
//        valueB.addFirst("A");
//        valueB.addFirst("D");
//        valueB.addFirst("E");
//
//        LinkedList<String> valueC = map.get("C");
//        valueC.addFirst("A");
//        valueC.addFirst("F");
//
//        LinkedList<String> valueD = map.get("D");
//        valueD.add("B");
//
//        LinkedList<String> valueE = map.get("E");
//        valueE.add("B");
//
//        LinkedList<String> valueF = map.get("F");
//        valueF.add("C");
//
//
//        dfs(map, "A");
//    }
//
//    private static void dfs(HashMap<String, LinkedList<String>> map, String start) {
//        Stack<String> stack = new Stack<>();
//        List<String> visited = new ArrayList<>();
//
//        stack.push(start);
//
//        while (!stack.isEmpty()) {
//            String vertex = stack.pop();
//            visited.add(vertex);
//
//            LinkedList<String> neighbors = map.get(vertex);
//            for (String s : neighbors) {
//                if (!visited.contains(s)) {
//                    stack.push(s);
//                }
//            }
//        }
//        System.out.println(visited);
//    }
//}

// DFS 재귀

//public class Main {
//    public static void main(String[] args) {
//        HashMap<String, LinkedList<String>> map = new HashMap<>();
//        map.put("A", new LinkedList<>());
//        map.put("B", new LinkedList<>());
//        map.put("C", new LinkedList<>());
//        map.put("D", new LinkedList<>());
//        map.put("E", new LinkedList<>());
//        map.put("F", new LinkedList<>());
//
//        LinkedList<String> valueA = map.get("A");
//        valueA.add("B");
//        valueA.add("C");
//
//        LinkedList<String> valueB = map.get("B");
//        valueB.add("A");
//        valueB.add("D");
//        valueB.add("E");
//
//        LinkedList<String> valueC = map.get("C");
//        valueC.add("A");
//        valueC.add("F");
//
//        LinkedList<String> valueD = map.get("D");
//        valueD.add("B");
//
//        LinkedList<String> valueE = map.get("E");
//        valueE.add("B");
//
//        LinkedList<String> valueF = map.get("F");
//        valueF.add("C");
//
//
//        dfsRepeat(map, "A", new ArrayList<>());
//    }
//
//    // DFS 재귀
//    private static void dfsRepeat(HashMap<String, LinkedList<String>> map, String node, List<String> visit) {
//        // 방문한 것 추가
//        visit.add(node);
//        System.out.println(node);
//
//        LinkedList<String> neighbors = map.get(node);
//        for (String neighbor : neighbors) if (!visit.contains(neighbor)) dfsRepeat(map, neighbor, visit);
//     }

//    private static void dfs(HashMap<String, LinkedList<String>> map, String start) {
//        Stack<String> stack = new Stack<>();
//        List<String> visited = new ArrayList<>();
//
//        stack.push(start);
//
//        while (!stack.isEmpty()) {
//            String vertex = stack.pop();
//            visited.add(vertex);
//
//            LinkedList<String> neighbors = map.get(vertex);
//            for (String s : neighbors) {
//                if (!visited.contains(s)) {
//                    stack.push(s);
//                }
//            }
//        }
//        System.out.println(visited);
//    }
//}

import java.util.*;

//class Solution {
//    private static class Operation {
//        int from, to;
//
//        Operation(int from, int to) {
//            this.from = from;
//            this.to = to;
//        }
//    }
//
//    public static void main(String[] args) {
//        int n = 4; // 원판 개수
//        Stack<Integer>[] tower = new Stack[n+1];
//        for (int i = 0; i <= n; i++) tower[i] = new Stack<>();
//        for (int i = n; i >= 1; i--) tower[0].push(i);
//
//        List<Operation> operations = hanoi(n, tower, 0, 1, 2);
//        for (Operation op : operations) System.out.println("원판을 " + (op.from+1) + "번 기둥에서 " + (op.to+1) + "번 기둥으로 이동");
//    }
//
//    private static List<Operation> hanoi(int n, Stack<Integer>[] tower, int from, int mid, int to) {
//        List<Operation> operations = new ArrayList<>();
//        if (n > 0) {
//            operations.addAll(hanoi(n - 1, tower, from, to, mid));
//            tower[to].push(tower[from].pop());
//            operations.add(new Operation(from, to));
//            operations.addAll(hanoi(n - 1, tower, mid, from, to));
//        }
//        return operations;
//    }
//}

// 최단 경로
//public class Main {
//    public static void main(String[] args) {
//        HashMap<String, LinkedList<String>> map = new HashMap<>();
//        // 기본 값으로 데이터 넣는 법
//        map.put("A", new LinkedList<>(Arrays.asList("B", "D")));
//        map.put("B", new LinkedList<>(Arrays.asList("A", "E")));
//        map.put("C", new LinkedList<>(Arrays.asList("D", "E", "G")));
//        map.put("D", new LinkedList<>(Arrays.asList("A", "C")));
//        map.put("E", new LinkedList<>(Arrays.asList("B", "F")));
//        map.put("F", new LinkedList<>(Arrays.asList("E", "H")));
//        map.put("G", new LinkedList<>(List.of("C")));
//        map.put("H", new LinkedList<>(List.of("F")));
//
//        List<String> bfsPath = bfsShortPath(map, "A", "H");
//        System.out.println(bfsPath);
//
//        List<String> dfsPath = new ArrayList<>();
//        dfsShortPath(map, "A", "H", new HashSet<>(), dfsPath);
//        System.out.println(dfsPath);
//    }
//
//    // bfs 최단 경로
//    private static List<String> bfsShortPath(HashMap<String, LinkedList<String>> map, String start, String end) {
//        // 부모 노드용 해쉬맵 생성
//        HashMap<String, String> parent = new HashMap<>();
//        // bfs니까 큐 생성
//        Queue<String> queue = new LinkedList<>();
//        // 큐에 시작값 넣어줌
//        queue.add(start);
//        // 부모용 노드에 시작값, 초기값 null을 줌
//        parent.put(start, null);
//
//        while (!queue.isEmpty()) {
//            // 노드 선언하면서 하나 뺌
//            String node = queue.poll();
//            // 뺀게 end랑 같으면 buildPath 메서드 실행
//            if (node.equals(end)) return buildPath(parent, end);
//
//            // 노드 불러옴
//            LinkedList<String> childs = map.get(node);
//            // 하나씩 던짐
//            for (String child : childs) {
//                // 기존 맵에서 키 같은게 없다면
//                if (!parent.containsKey(child)) {
//                    // 해쉬맵에 데이터 추가
//                    parent.put(child, node);
//                    // 큐에 데이터 추가
//                    queue.add(child);
//                }
//            }
//        }
//        return null;
//    }
//
//    private static List<String> buildPath(HashMap<String, String> parent, String end) {
//        // 경로 저장용 변수 추가
//        List<String> path = new ArrayList<>();
//        // 변수 node에 end를 초기값으로 줌, node가 true일 동안 반복 null이 되면 종료, node값을 parent로 불러와서 부모값으로 업데이트, 리스트의 맨 앞에 추가
//        for (String node = end; node != null; node = parent.get(node)) path.add(0, node);
//        return path;
//    }
//
//    // dfs 최단 경로
//    private static boolean dfsShortPath(HashMap<String, LinkedList<String>> map, String start, String end, Set<String> visited, List<String> path) {
//        // 경로에 값 넣음
//        path.add(start);
//        // 시작값이 끝과 같다면 true 반환 (경로를 찾은 것)
//        if (start.equals(end)) return true;
//
//        // 방문 했다고 추가
//        visited.add(start);
//
//        // start로 부모 노드 불러옴
//        LinkedList<String> children = map.get(start);
//        // 자식 노드를 하나씩 던져서 자식 노드가 아직 방문 되지 않았고, start에 자식 노드의 노드 하나로 줘서 재귀 돌림 , dfsShortPath가 true일 때( = end까지의 경로를 찾을 수 있으면) true를 반환
//        for (String child : children) if (!visited.contains(child) && dfsShortPath(map, child, end, visited, path)) return true;
//        // 만약 현재 노드에서 end 노드까지의 경로를 찾지 못했다면, 경로에서 현재 노드를 제거
//        path.remove(path.size() - 1);
//        // 모든 자식 노드를 탐색하고 경로를 찾지 못하면 false를 반환
//        return false;
//    }
//}

// 색종이 만들기
public class Main {
    public static int countWhite = 0;
    public static int countBlue = 0;
    public static int[][] board;

    public static void main(String[] args) {
        board = new int[][] {
                {1, 1, 0, 0, 0, 0, 1, 1},
                {1, 1, 0, 0, 0, 0, 1, 1},
                {0, 0, 0, 0, 1, 1, 0, 0},
                {0, 0, 0, 0, 1, 1, 0, 0},
                {1, 0, 0, 0, 1, 1, 1, 1},
                {0, 1, 0, 0, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 1, 1, 1},
        };

        cutPaper(0, 0, board.length);
        System.out.println(countWhite);
        System.out.println(countBlue);
    }

    private static void cutPaper(int row, int col, int paperSize) {
        if (isSameColor(row, col, paperSize)) {
            if (board[row][col] == 0) countWhite++;
            else countBlue++;
            return;
        }

        int halfSize = paperSize / 2;

        cutPaper(row, col, halfSize);
        cutPaper(row, col + halfSize, halfSize);
        cutPaper(row +  halfSize, col, halfSize);
        cutPaper(row + halfSize, col + halfSize, halfSize);
    }

    private static boolean isSameColor(int row, int col, int paperSize) {
        int gijunColor = board[row][col];

        for (int i = row; i < row + paperSize; i++) {
            for (int j = col; j < col + paperSize; j++) {
                if (gijunColor != board[i][j]) return false;
            }
        }
        return true;
    }
}